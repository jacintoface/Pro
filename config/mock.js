var express = require("express");
var app = express();

app.all("*", function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    next();
});

var appDate = require('../data.json')

var seller = appDate.seller;
var goods = appDate.goods;
var ratings = appDate.ratings;

var apiRoutes = express.Router();

apiRoutes.get("/seller",function (req,res) {
    res.json({
        errno:0,
        data:seller
    })
})

apiRoutes.get("/goods",function (req,res) {
    res.json({
        errno:0,
        data:goods
    })
})

apiRoutes.get("/ratings",function (req,res) {
    res.json({
        errno:0,
        data:ratings
    })
})

app.use("/api",apiRoutes);

app.listen(3011,function () {
    console.log("成功监听3011端口")
})