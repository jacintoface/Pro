import {handleActions,createAction} from 'redux-actions'
import axios from 'axios'

export const SELLER_INFO = createAction("SELLER_INFO");
export function saveSeller (data) {
    return (dispatch,getState) => {
       dispatch(SELLER_INFO(data))
    }
}

export const STORAGE_GOODS = createAction('STORAGE_GOODS')
export function storageGoods (goods) {
    return (dispatch,getState) => {
        dispatch(STORAGE_GOODS(goods))
    }
}

export const CHANGE_FOOD = createAction('CHANGE_FOOD')
export function changeFood (foodname,menu,count=1,foodmessage,isAdd = true) {
    return (dispatch,getState) => {
        dispatch(CHANGE_FOOD({foodname,count,isAdd,menu,foodmessage}))
    }
}

export const CART_ANIMATED = createAction('CART_ANIMATED')
export function doAnimate (params) {
    return (dispatch,getState) => {
        dispatch(CART_ANIMATED(params))
    }
}