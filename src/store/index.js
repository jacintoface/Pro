import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import reducer from '../reducer'
const enhancer = applyMiddleware(thunk,createLogger());
const configStore = () => {
    let store = createStore(reducer,enhancer)
    return store
}
export default configStore
