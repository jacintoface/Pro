import { handleActions } from 'redux-actions'

import {check,contains} from '@utils/index'

let food = {

}

export default handleActions({
    CHANGE_FOOD (state,actions) {
        let copyState = Object.assign({},state);
        let currentCount = 0;
        let changeObject = {
            [actions.payload.foodname]:{
                munu:actions.payload.menu,
                foodmessage:actions.payload.foodmessage
            }
        }
        
        /*如果没有这个food名或者food名字相同但是对应的店不相同*/
        if(!contains(state,actions.payload.foodname) || copyState[actions.payload.foodname].munu !== actions.payload.menu) {
            changeObject[actions.payload.foodname].count = 0
            currentCount = 0
        }else {
            currentCount = copyState[actions.payload.foodname].count;
        }
        
        if(actions.payload.isAdd) {
            currentCount += actions.payload.count;
        }else {
            currentCount -= actions.payload.count;
        }
    
        changeObject[actions.payload.foodname].count = currentCount;
        
        return {
            ...state,
            ...changeObject
        }
    }
}, food)
