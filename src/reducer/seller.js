import { handleActions } from 'redux-actions'

let initState = {
    seller: {}
}

export default handleActions({
    SELLER_INFO: (state, action) => {
        return {
            ...state,
            seller: Object.assign({}, state.seller, action.payload)
        }
    }
}, initState)
