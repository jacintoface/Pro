import {combineReducers} from 'redux'
 import goods from './goods'
// import ratings from './ratings'
import seller from './seller'
import food from './food'

export default combineReducers({
    seller,
    goods,
    food
})