import { handleActions } from 'redux-actions'

let initState = {
    goods: {},
    animatelist:[]
}

export default handleActions({
    STORAGE_GOODS (state,actions) {
        return {
            ...state,
            goods:actions.payload
        }
    },
    CART_ANIMATED (state,actions) {
        console.log(state,'------',actions)
        let animate = state.animatelist.slice();
        animate.push(actions.payload)
        return {
            ...state,
            animatelist:animate
        }
    }
}, initState)
