import React,{Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink,
    Switch,
    Redirect
} from 'react-router-dom'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import axios from 'axios'
import './main.scss'

export default class App extends Component {
    constructor (props) {
        super(props)
    }
    
    state = {
        seller:{}
    }
    
    
    
    componentWillMount () {
    
    }
    
    render () {
        return (
            <div>
                    <Router>
                        <div>
                           <div className="route-list">
                               <NavLink to='/goods' className={'link-item'}>商品</NavLink>
                               <NavLink to='/ratings' className={'link-item'}>评价</NavLink>
                               <NavLink to='/seller' className={'link-item'}>商家</NavLink>
                           </div>
                            <Switch>
                                
                                <Route path='/' exact render={() => (<Redirect to="/goods"></Redirect>)}></Route>
                                <Route path='/goods' render={()=>{
                                    return (
                                        <div>
                                            1
                                        </div>
                                    )
                                }}></Route>
                                <Route path='/ratings' render={()=>(
                                    <div>
                                        2
                                    </div>
                                )}></Route>
                                <Route path='/seller' render={()=>(
                                    <div>
                                        3
                                    </div>
                                )}></Route>
                            </Switch>
                        </div>
                    </Router>
            </div>
        )
    }
}

// const MapStateToProps = (state) => {
//     return {
//         seller:state.seller
//     }
// }
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         actions:bindActionCreators({...actions},dispatch)
//     }
// }
//
// export default connect(MapStateToProps,mapDispatchToProps)(App)