import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import { Provider } from 'react-redux'
import createStore from './store'
import registerServiceWorker from './registerServiceWorker'
import "@static/css/reset"
import "@static/css/font"

let store = createStore()

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
registerServiceWorker()
