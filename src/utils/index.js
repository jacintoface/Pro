export function check (obj) {
    if(obj === null || obj === undefined || obj === false) {
        return false
    }
    if(typeof obj === 'string' && obj === "") {
        return false
    }
     if(Object.prototype.toString.call(obj) === '[object Object]') {
        for(var key in obj){
            return true
        }
        return false
     }
    
    if(Object.prototype.toString.call(obj) === '[object Array]') {
        if(obj.length === 0) {
            return false
        }
        return true
    }
}


export function contains (obj,item) {
    if(Object.prototype.toString.call(obj) === '[object Object]') {
        if(Reflect.has(obj,item)) return true
    }
    
    if(Object.prototype.toString.call(obj) === '[object Array]') {
        return obj.indexOf(item) > 0 ?true:false
    }
    if(obj === item) {
        return true
    }
    return false
}